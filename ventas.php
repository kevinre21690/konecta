<?php
include("conexion.php");
$id_producto = $_GET['id']; //se obtiene el id del producto 
?>

<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style.css">
        <title>Konecta Tienda</title>
    </head>

    <body>
        <main class="main" id="main">
            <div class="content_form" id="content_form">
                <form class="form" action="actualizarVenta.php" method="POST">
                    <p class="title_form">formulario de ventas</p>
                    <div class="campos_form">
                        <input type="number" name= "id" value="<?php echo $id_producto?>"> 
                    </div>
                    <div class="campos_form">
                        <input type="number" placeholder="cantidad" name= "cantidad">
                    </div>
                    <div class="campos_form">
                        <button type="submit">Vender</button>
                        <button><a href="index.php">Volver</a></button>
                    </div>
                </form>
            </div>
            <div class="content_table" id="content_table">
                <div class="table">
                    <p class="title_table"> Tabla de ventas</p>
                    <table class="table_products">
                        <tr class="title_colums">
                            <th>Id</th>
                            <th>N. de producto</th>
                            <th>Referencia</th>
                            <th>Categoría</th>
                            <th>Precio</th>
                            <th>Peso</th>
                            <th>vendidos</th>
                            <th>Fecha venta</th>
                        </tr>
                        <?php
                        $sentenciaSqlBusqueda = "SELECT c.id,c.nombre,c.referencia,c.precio,c.peso,c.categoria,c.stock,c.fechaVenta,a.nombre as idCategoria FROM ventas c JOIN categoria a on c.id=a.id";
                        $result = mysqli_query($conexion,$sentenciaSqlBusqueda);
                        while ($rows= mysqli_fetch_assoc($result)) {   
                        ?>
                        <tr class="dates_tables">
                            <td><?php echo $rows["id"]?></td>
                            <td><?php echo $rows["nombre"]?></td>
                            <td><?php echo $rows["referencia"]?></td>
                            <td><?php echo $rows["precio"]?></td>
                            <td><?php echo $rows["peso"]?></td>
                            <td><?php echo $rows["idCategoria"]?></td>
                            <td><?php echo $rows["stock"]?></td>
                            <td><?php echo $rows["fechaVenta"]?></td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </main>

    </body>



</html>