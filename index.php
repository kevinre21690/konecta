<?php 
include("conexion.php");
?>
<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style.css">
        <title>Konecta Tienda</title>
    </head>

    <body>
        <main class="main" id="main">
            <div class="content_form" id="content_form">
                <form class="form" action="insertar.php" method="POST">
                    <p class="title_form">formulario de productos</p>
                    <div class="campos_form">
                        <input type="text" placeholder="Nombre del producto" name="nombre">
                    </div>
                    <div class="campos_form">
                        <input type="text" placeholder="referencia del producto" name="referencia">
                    </div>
                    <div class="campos_form">
                        <select name="categoria" id="">
                        <option disabled selected>Categoría de producto</option>
                            <?php
                                $sentenciaSqlBusqueda = "SELECT id,nombre FROM categoria";
                                $result = mysqli_query($conexion,$sentenciaSqlBusqueda);
                                while ($rows= mysqli_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo $rows['id']?>"><?php echo $rows['nombre'];?></option>
                            
                            <?php } ?>
                        </select>
                    </div>
                    <div class="campos_form">
                        <input type="number" placeholder="valor del producto(USD)" name="precio">
                    </div>
                    <div class="campos_form">
                        <input type="number" placeholder="peso del producto(Kg)" name="peso">
                    </div>
                    <div class="campos_form">
                        <input type="number" placeholder="Stock" name="stock">
                    </div>
                    <div class="campos_form">
                        <input class="date" type="date" placeholder="fecha de registro" name="fecha">
                    </div>
                    <div class="campos_form">
                        <button type="submit">Guardar</button>
                    </div>
                </form>
            </div>
            <div class="content_table" id="content_table">
                <div class="table">
                    <p class="title_table"> Tabla de productos</p>
                    <table class="table_products">
                        <tr class="title_colums">
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Referencia</th>
                            <th>Precio</th>
                            <th>Peso</th>
                            <th>Categoria</th>
                            <th>Stock</th>
                            <th>Fecha</th>
                        </tr>
                        <?php $sentenciaSqlBusquedaProduct = "SELECT c.id,c.nombre,c.referencia,c.precio,c.peso,c.categoria,c.stock,c.fecha,a.nombre as idCategoria FROM cafeteria c JOIN categoria a on c.id=a.id";
                                $resultado = mysqli_query($conexion,$sentenciaSqlBusquedaProduct);
                                while ($filas= mysqli_fetch_assoc($resultado)) {
                        ?>
                        <tr class="dates_tables">
                            <td class="date"><?php echo $filas["id"]?></td>
                            <td class="date"><?php echo $filas["nombre"]?></td>
                            <td class="date"><?php echo $filas["referencia"]?></td>
                            <td class="date"><?php echo $filas["precio"]?></td>
                            <td class="date"><?php echo $filas["peso"]?></td>
                            <td class="date"><?php echo $filas["idCategoria"]?></td>
                            <td class="date"><?php echo $filas["stock"]?></td>
                            <td class="date"><?php echo $filas["fecha"]?></td>
                            <td><button class="btn_table"><a href="delete.php?id=<?php echo $filas["id"]?>">Borrar</a></button></td>
                            <td><button class="btn_table"><a href="actualizar.php?id=<?php echo $filas["id"]?>">editar</a></button></td>
                            <td><button class="btn_table"><a href="ventas.php?id=<?php echo $filas["id"]?>">vender</a></button></td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </main>

    </body>



</html>