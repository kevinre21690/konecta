<?php
include("conexion.php");

$id_producto = $_GET['id'];
//se consulta la tabla  para extraer los datos del producto a actualizar
$sentenciaSqlBusqueda = "SELECT * FROM cafeteria WHERE  id = '$id_producto'";
$result = mysqli_query($conexion,$sentenciaSqlBusqueda);
$row = mysqli_fetch_assoc($result);
//pintar en una tabla esos campos y luego redirecinar ese formuario a update.phpmailer
//crear boton de volver
?>

<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style.css">
        <title>Konecta Tienda</title>
    </head>

    <body>
        <main class="main" id="main">
            <div class="content_form_update" id="content_form">
                <form class="form" action="update.php" method="POST">
                    <p>id</p>
                    <div class="campos_form_update">
                        <input type="text" value="<?php echo $row['id'];?> " placeholder="id del producto" name="id" readonly>
                    </div>
                    <p>nombre</p>
                    <div class="campos_form_update">
                        <input type="text" value="<?php echo $row['nombre'];?> " placeholder="Nombre del producto" name="nombre">
                    </div>
                     <p>referencia</p>
                    <div class="campos_form_update">
                        <input type="text" value="<?php echo $row['referencia'];?> "placeholder="referencia del producto" name="referencia" >
                    </div>
                    <p>categoria</p>
                    <div class="campos_form_update">
                        <select name="categoria" id="">
                     
                            <?php
                                $sentenciaSqlBusqueda = "SELECT id,nombre FROM categoria";
                                $result = mysqli_query($conexion,$sentenciaSqlBusqueda);
                                while ($rows= mysqli_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo $rows['id']?>"><?php echo $rows['nombre'];?></option>
                            
                            <?php } ?>
                        </select>
                    </div>
                    <p>precio</p>
                    <div class="campos_form_update">
                        <input type="text" value="<?php echo $row['precio'];?> " placeholder="valor del producto(USD)" name="precio">
                    </div>
                    <p>peso</p>
                    <div class="campos_form_update">
                        <input type="text" value="<?php echo $row['peso'];?> " placeholder="peso del producto(Kg)" name="peso">
                    </div>
                    <p>stock</p>
                    <div class="campos_form_update">
                        <input type="text" value="<?php echo $row['stock'];?> " placeholder="Stock" name="stock">
                    </div>
                    <p>fecha</p>
                    <div class="campos_form_update">
                        <input class="date_update" value="<?php echo $row['fecha'];?> " type="text" placeholder="fecha de registro" name="fecha">
                    </div>
                    <div class="campos_form_update">
                        <button type="submit">Actualizar</button>
                    </div>
                </form>
            </div>
        </main>

    </body>

</html>